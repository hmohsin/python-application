# python-app-docker-demo
This demo shows two steps.

+ Install `docker-ce` on Amazon-linux
+ Build and run a simple docker image with a python+flask web application.

## Install docker-ce on Centos 7
Refer to https://docs.aws.amazon.com/AmazonECS/latest/developerguide/docker-basics.html
You can also find [other OS installation docs from here](https://docs.docker.com/engine/installation).


Other commands:

+ check docker status 
```
sudo systemctl status docker.service
```

+ stop docker 
```
sudo systemctl stop docker
```

+ uninstall docker-ce
```
sudo yum remove docker-ce
```

+ remove all images, container, volumes
```
sudo rm -rf /var/lib/docker
```

## Build/Run a simple python+flask docker web app 

#### Clone repo on EC2 Instance in /opt folder

```
cd /opt
sudo git clone https://hmohsin@bitbucket.org/hmohsin/python-application.git
```

#### Build Docker image

```
cd python-application
sudo docker build -t python-application:latest .
```

#### Run your application using docker compose file
```
cd python-application
sudo docker-compose up -d
```


You can use `sudo docker ps` to list all running containers. 
```
$ sudo docker ps
CONTAINER ID        IMAGE                 COMMAND                  CREATED             STATUS              PORTS                    NAMES
52e5f8c631aa        python-application:latest   "python3 ./simple.py"   57 minutes ago      Up 57 minutes       0.0.0.0:8000->5000/tcp   python-application
```

`52e5f8c631aa` is the running container id. Some commands below are what you might need.

+ display logs in running container
```
    $ sudo docker logs 52e5f8c631aa
        Serving Flask app "simple" (lazy loading)
        Environment: production
        WARNING: This is a development server. Do not use it in a production deployment.
        Use a production WSGI server instead.
        Debug mode: on
        Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
        Restarting with stat
        Debugger is active!
        Debugger PIN: 924-357-821
```

+ stop your container
    ```
    $ sudo docker-compose down
    ```

#### Test your application
```
$ curl http://localhost:8000
    Date 2020-05-19 07:26:43
    Latitute -34.3862
    Longitude 109.4627
```