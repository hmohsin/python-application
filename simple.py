#!/usr/bin/env python3
import requests
import time
from datetime import datetime as Date
from flask import Flask

# from osmapi import OsmApi

app = Flask(__name__)

@app.route("/")
def index():
    response = requests.get("http://api.open-notify.org/iss-now.json")
    if response.status_code & 200 == 200:
        res = response.json()
        ts = int(res.get("timestamp"))
        date = Date.utcfromtimestamp(ts).strftime("%Y-%m-%d %H:%M:%S")
        location = res.get("iss_position")
        lat = location.get("latitude")
        lon = location.get("longitude")

        return "Date {} \nLatitute {} \nLongitude {}".format(date, lat, lon)

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')